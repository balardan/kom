CC=g++

# Decision version of the problem
MIN_VALUE = -DMIN_VALUE # Search for the first solution that has minimum value, not the best

ITEM_RATIO_SORT = -DITEM_RATIO_SORT # Sort items based on ratio 
ITEM_RATIO_HEURISTIC = ${ITEM_RATIO_SORT} -DITEM_RATIO_HEURISTIC # Use ratio heuristic // Greedy heurestic
ITEM_RATIO_REDUX = ${ITEM_RATIO_HEURISTIC} -DITEM_RATIO_REDUX # Sort items based on ratio, compare result with single biggest item // Greedy heurestic, REDUX
CHECK_BRANCH_VALUE = -DCHECK_BRANCH_VALUE # If branch value can't fill the bag up to minimum, cut it // Branch and Bounds
DYNAMIC_PROGRAMMING = -DDYNAMIC_PROGRAMMING # Dynamic programming, decomposition by price

# Input validation, has little to no effect to non random data
VALUE_CHECK = -DVALUE_CHECK # Check if items have sufficient value
WEIGHT_CHECK = -DWEIGHT_CHECK # Filter out items that can't fit in
ZERO_VALUE_CHECK = -DZERO_VALUE_CHECK # Check if bag min value is greater than 0
DISABLE_OUTPUT = -DDISABLE_OUTPUT # Disables output of the program, so the algorithm is not slowed down by IO

EPSILON=0
EPSILON_VALUE=-DEPSILON_VALUE=${EPSILON} # Epsilon for dynamic programming

OUT_NAME = knapsack.out
IN_FILE = NK/NK32_inst.dat

COMMON_ARGS = --std=c++20 main.cpp -o $(OUT_NAME)
BUILD_OPTIMIZATION = -O3
BASIC_OPTIMIZATION = $(DISABLE_OUTPUT)
# BASIC_OPTIMIZATION = $(MIN_VALUE) $(DISABLE_OUTPUT)
OPTIMIZATION = $(BASIC_OPTIMIZATION) $(VALUE_CHECK) $(WEIGHT_CHECK) $(ITEM_RATIO_SORT) $(ZERO_VALUE_CHECK)


clean:
	rm -f -- $(OUT_NAME)

# build-brute: clean
# 	$(CC) $(BUILD_OPTIMIZATION) $(BASIC_OPTIMIZATION) $(COMMON_ARGS)

# build-brute-optimized: clean
# 	$(CC) $(BUILD_OPTIMIZATION) $(OPTIMIZATION) $(COMMON_ARGS)

# build-bnb-optimized: clean
# 	$(CC) $(BUILD_OPTIMIZATION) $(CHECK_BRANCH_VALUE) $(OPTIMIZATION) $(COMMON_ARGS)

build-bnb: clean
	$(CC) $(BUILD_OPTIMIZATION) $(CHECK_BRANCH_VALUE) $(COMMON_ARGS)

build-heuristic: clean
	$(CC) $(BUILD_OPTIMIZATION) $(ITEM_RATIO_HEURISTIC) $(COMMON_ARGS)

build-redux: clean
	$(CC) $(BUILD_OPTIMIZATION) $(ITEM_RATIO_REDUX) $(COMMON_ARGS)

build-dp: clean
	$(CC) $(BUILD_OPTIMIZATION) ${EPSILON_VALUE} $(DYNAMIC_PROGRAMMING) $(COMMON_ARGS)


test:
	@cat $(IN_FILE) | ./$(OUT_NAME)

# build-test-brute: build-brute test

# build-test-brute-optimized: build-brute-optimized test

# build-test-bnb-optimized: build-bnb-optimized test

main.o: main.cpp
	$(CC) --std=c++17 -Wall -g -c $(OUT_NAME)