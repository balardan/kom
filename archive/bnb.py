from sys import stdin
import copy

class Thing:
    def __repr__(self):
        return f"{self.id}: W:{self.weight},V:{self.value},R:{self.ratio}"
    def __init__(self, id, weight, value):
        self.id = int(id)
        self.weight = int(weight)
        self.value = int(value)
        self.ratio = self.value / self.weight

class Node:
    def __init__(self, level, profit, weight, bound):
        self.level = int(level)
        self.profit = int(profit)
        self.weight = int(weight)
        self.bound = int(bound)

def bound(node, n, capacity, things):
    if node.weight >= capacity:
        return 0
    profit_bound = node.profit
    j = node.level + 1
    tot_weight = node.weight

    while j < n and tot_weight + things[j].weight <= capacity:
        tot_weight += things[j].weight
        profit_bound += things[j].value
        j += 1
    
    if j < n:
        profit_bound += (capacity - tot_weight) * things[j].value / things[j].weight

    return profit_bound

def solve(things, capacity, min_val):
    queue = []
    u = Node(-1, 0, 0, 0)
    n = len(things)
    queue.append(u)
    max_profit = 0
    while len(queue) > 0:
        v = Node(-1, 0, 0, 0)
        u = queue[0]
        queue.pop(0)

        if u.level == -1:
            v.level = 0
        
        if u.level == n-1:
            continue

        v.level = u.level + 1
        v.weight = u.weight + things[v.level].weight
        v.profit = u.profit + things[v.level].value

        if v.weight <= capacity and v.profit > max_profit:
            max_profit = v.profit

        v.bound = bound(v, n, capacity, things)

        if v.bound > max_profit:
            queue.append(v)

        v.weight = u.weight
        v.profit = u.profit
        v.bound = bound(v, n, capacity, things)
        if v.bound > max_profit:
            queue.append(v)
    return max_profit
    

def evaluate(input_line):
    id, args_num, capacity, min_val, *thingsData = input_line.split()
    things = []
    for i in range(0, int(args_num) * 2, 2):
        thing = Thing(i/2+1, thingsData[i], thingsData[i+1])
        things.append(thing)
    things.sort(key=lambda x: x.ratio, reverse=True)
    

    # print('\n'.join(map(Thing.__repr__,things)))
    print(f"{int(id)*-1} {args_num} {solve(things, int(capacity), int(min_val))}")

for line in stdin:
    evaluate(line)

# evaluate(input())

