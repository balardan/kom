from sys import stdin

# Implement knapsack problem solution using branch and bound method
def knapsack(items, capacity):
    # items is a list of tuples (value, weight)
    # capacity is the maximum weight the knapsack can hold
    # return the maximum value that can be put in the knapsack
    # using branch and bound method
    def bound(items, capacity, index):
        # return the maximum value that can be put in the knapsack
        # using branch and bound method
        if index >= len(items) or capacity <= 0:
            return 0
        if items[index][1] > capacity:
            return bound(items, capacity, index + 1)
        return max(items[index][0] + bound(items, capacity - items[index][1], index + 1),
                   bound(items, capacity, index + 1))

    def knapsack_bound(items, capacity):
        # return the maximum value that can be put in the knapsack
        # using branch and bound method
        l = len(items)
        return bound(items, capacity, 0)

    items.sort(key=lambda x: x[1] / x[0], reverse=True)
    return knapsack_bound(items, capacity)

def evaluate(input_line):
    id, args_num, capacity, min_val, *thingsData = input_line.split()
    items = []
    for i in range(0, int(args_num) * 2, 2):
        item = [int(thingsData[i]), int(thingsData[i+1])]
        items.append(item)
    print(knapsack(items, int(capacity)))

for line in stdin:
    evaluate(line)