from sys import stdin
import copy

def print_thing(thing):
    return f"{thing.weight}: W:{thing.weight},V:{thing.value}"

class Thing:
    def __repr__(self):
        return f"{self.id}: W:{self.weight},V:{self.value}"
    def __init__(self, id, weight, value):
        self.id = int(id)
        self.weight = int(weight)
        self.value = int(value)

class Bag:
    def __repr__(self):
        return f"+ W:{self.weight}, C:{self.capacity}\n -" + '\n -'.join(map(Thing.__str__,self.things))
    def __init__(self, capacity):
        self.capacity = int(capacity)
        self.things = []
        self.weight = 0

    def add_thing(self, thing):
        if self.weight + thing.weight <= self.capacity:
            self.things.append(thing)
            self.weight += thing.weight
            return True
        return False

def add_to_bag(bag, thing, things):
    res = bag.add_thing(thing)
    if not res:
        return 0
    max_val = 0
    for i, thing in enumerate(things):
        new_things = things.copy()
        new_things.pop(i)
        new_bag = copy.deepcopy(bag)
        val = add_to_bag(new_bag, thing, new_things)
        if val > max_val:
            max_val = val
    return thing.value + max_val

def get_biggest_value(bag, things):
    max_val = 0
    for thing in things:
        new_bag = copy.deepcopy(bag)
        val = add_to_bag(new_bag, thing, things)
        print(val)
        if val > max_val:
            max_val = val
    return max_val

def evaluate(input_line):
    id, args_num, capacity, bs, *thingsData = input_line.split()
    things = []
    for i in range(0, int(args_num) * 2, 2):
        thing = Thing(i/2+1, thingsData[i], thingsData[i+1])
        things.append(thing)

    bag = Bag(capacity)
    print(capacity)

    print(bag)

    print('\n'.join(map(Thing.__repr__,things)))
    print(get_biggest_value(bag, things))

# for line in stdin:
#     evaluate(line)

evaluate(input())

