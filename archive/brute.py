from sys import stdin
import copy

class Thing:
    def __repr__(self):
        return f"{self.id}: W:{self.weight},V:{self.value}"
    def __init__(self, id, weight, value):
        self.id = int(id)
        self.weight = int(weight)
        self.value = int(value)

def solve(things, capacity, min_val, used_things, curr_val):
    if len(things) == 0 or capacity == 0:
        return 0
    new_things = things.copy()
    del new_things[-1]
    thing = things[-1]
    used_things[thing.id - 1] = 0
    if (thing.weight > capacity):
        return solve(new_things, capacity, min_val, used_things, curr_val)
    else:
        added = thing.value + solve(new_things, capacity - thing.weight, min_val, used_things, curr_val + thing.value)
        not_added = solve(new_things, capacity, min_val, used_things, curr_val)
        if added > not_added:
            used_things[thing.id - 1] = 1
            return added
        else:
            return not_added
    

def evaluate(input_line):
    id, args_num, capacity, min_val, *thingsData = input_line.split()
    things = []
    for i in range(0, int(args_num) * 2, 2):
        thing = Thing(i/2+1, thingsData[i], thingsData[i+1])
        things.append(thing)

    # print(capacity, min_val)


    # print('\n'.join(map(Thing.__repr__,things)))
    used_things = [0]*int(args_num)
    print(f"{int(id)*-1} {args_num} {solve(things, int(capacity), int(min_val), used_things, 0)}")
    # print(used_things)

for line in stdin:
    evaluate(line)

# evaluate(input())

