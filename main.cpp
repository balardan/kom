#include <cmath>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;

#ifndef EPSILON_VALUE
#define EPSILON_VALUE 0
#endif

struct Item
{
    int id;
    int weight;
    int price;
};

bool compareItemsByRatio(Item a, Item b)
{
    if (a.weight == 0 || b.weight == 0)
    {
        return a.price < b.price;
    }
    return (a.price / a.weight) < (b.price / b.weight);
}

bool compareItemsById(Item a, Item b)
{
    return a.id < b.id;
}

typedef vector<vector<int>> DpArray;
typedef tuple<int, int> DpSolution;

struct InputLine
{
    int id;
    int bagItemCount;
    int capacity;
    int minVal;
    vector<Item> items;
    int _totalItemsValue;
    int _totalItemsWeight;
    Item _mostValuableItem;
    float _epsilonFactor;
};

struct Solution
{
    int inputId;
    int value;
    int weight;
    int lastIndex;
    int valueWentThrough;  // used for CHECK_BRANCH_VALUE
    int weightWentThrough; // used for CHECK_BRANCH_VALUE
    vector<bool> used;
};

Solution copySolution(Solution *sol)
{
    Solution res = {sol->inputId,
                    sol->value,
                    sol->weight,
                    sol->lastIndex,
                    sol->valueWentThrough,
                    sol->weightWentThrough,
                    vector(sol->used)};
    return res;
}

void solutionNotAddItem(Solution *solution, Item *item)
{
    solution->used[item->id] = false;
    solution->valueWentThrough += item->price;
    solution->weightWentThrough += item->weight;
}

void solutionAddItem(Solution *solution, Item *item)
{
    solution->used[item->id] = true;
    solution->value += item->price;
    solution->weight += item->weight;
    solution->valueWentThrough += item->price;
    solution->weightWentThrough += item->weight;
}

InputLine processLine()
{
    InputLine input;
    cin >> input.id >> input.bagItemCount >> input.capacity;
#ifdef MIN_VALUE
    cin >> input.minVal;
#else
    input.minVal = 0;
#endif
    vector<Item> items(0);
    input._totalItemsValue = 0;
    input._mostValuableItem = {-1, 0, 0};
    for (int i = 0; i < input.bagItemCount; ++i)
    {
        Item item;
        item.id = i;
        cin >> item.weight >> item.price;
        if (item.price > input._mostValuableItem.price && item.weight <= input.capacity)
        {
            input._mostValuableItem = item;
        }
#ifdef WEIGHT_CHECK
        if (item.weight <= input.capacity)
        {
#endif
#ifdef ITEM_RATIO_SORT
            items.insert(upper_bound(items.begin(), items.end(), item, compareItemsByRatio), item);
#else
        items.push_back(item);
#endif
            input._totalItemsValue += item.price;
#ifdef WEIGHT_CHECK
        }
#endif
    }
#ifdef DYNAMIC_PROGRAMMING
    Item emptyItem = {-1, 0, 0};
    items.insert(items.begin(), emptyItem);
    input.bagItemCount++;
#endif
    input.items = items;
    return input;
}

vector<InputLine> processInput()
{
    vector<InputLine> inputs;
    while (!cin.eof())
    {
        inputs.push_back(processLine());
    }
    return inputs;
}

void printSolution(Solution *sol)
{
    cout << sol->inputId << " " << sol->used.size() << " " << sol->value;
    for (bool item : sol->used)
    {
        cout << " " << (item ? 1 : 0);
    }
    cout << " " << endl;
}

void epsilonAdjustItems(InputLine *input)
{
    float factor = (input->_mostValuableItem.price * EPSILON_VALUE) / (input->items.size() - 1);
    if (factor < 1)
    {
        input->_epsilonFactor = 1;
        return;
    }
    input->_epsilonFactor = factor;
    input->_totalItemsValue = floor(input->_totalItemsValue / factor);
    for (Item &item : input->items)
    {
        item.price = floor(item.price / factor);
    }
}

vector<bool> backtrackDPItems(InputLine input, DpArray *arr, DpSolution sol)
{
    vector<bool> resultItems = vector<bool>(input.items.size(), false);
    int c = get<0>(sol);
    int i = get<1>(sol);
    while (i - 1 >= 0 && c >= 1)
    {
        auto item = input.items[i];
        if (arr->at(c)[i - 1] != arr->at(c)[i])
        {
            resultItems[i] = true;
            c -= item.price;
        }
        else
        {
            i--;
        }
    }

    input.items.erase(input.items.begin()); // remove temporary empty item
    resultItems.erase(resultItems.begin()); // remove temporary empty item

    return resultItems;
}

Solution solveDP(InputLine input)
{
    Solution solution = {input.id, 0, 0, 0};
    solution.used = vector<bool>(input.items.size(), false);
    DpArray arr;
    DpSolution sol = {0, 0};
    epsilonAdjustItems(&input);

    for (int c = 0; c <= input._totalItemsValue; ++c)
    {
        arr.push_back(vector<int>(input.items.size(), 0));
        arr[c][0] = INT_MAX;
    }
    arr[0][0] = 0;

    for (int i = 1; i < input.items.size(); ++i)
    {
        for (int c = 0; c <= input._totalItemsValue; ++c)
        {
            int nextC = c - input.items[i].price;
            if (nextC < 0)
            {
                arr[c][i] = arr[c][i - 1];
            }
            else
            {
                int b = arr[nextC][i - 1] == INT_MAX ? arr[nextC][i - 1] : arr[nextC][i - 1] + input.items[i].weight;
                arr[c][i] = min(arr[c][i - 1], b);
                if (arr[c][i] <= input.capacity && c > get<0>(sol) && i == input.items.size() - 1)
                {
                    sol = {c, i};
                }
            }
        }
    }

    solution.value = get<0>(sol) * input._epsilonFactor;
    solution.weight = arr[get<0>(sol)][get<1>(sol)];
    solution.used = backtrackDPItems(input, &arr, sol);
    return solution;
}

Solution solveIterative(InputLine input)
{
    Solution emptySolution = {input.id, 0, 0, 0};
    emptySolution.used = vector<bool>(input.items.size(), false);
    Solution max = copySolution(&emptySolution);
    max.used = vector<bool>(input.items.size(), false);

#if defined(ZERO_VALUE_CHECK) && defined(MIN_VALUE)
    if (input.minVal == 0)
    {
        return max;
    }
#endif
#if defined(VALUE_CHECK) && defined(MIN_VALUE)
    if (input._totalItemsValue < input.minVal)
    {
        return max;
    }
#endif

    queue<Solution> q;
    max.lastIndex = -1;
    q.push(copySolution(&max));

    while (!q.empty())
    {
        Solution sol = q.front();
        q.pop();

#ifdef MIN_VALUE
        if (sol.value >= input.minVal)
        {
            return sol;
        }
#endif

#if defined(CHECK_BRANCH_VALUE) && defined(MIN_VALUE)
        // Solution can't possibly fulfil minimum expected value
        if (sol.value + (input._totalItemsValue - sol.valueWentThrough) < input.minVal)
        {
            continue;
        }
#endif
#ifdef CHECK_BRANCH_VALUE
        // Solution can't be better than best found solution
        if (sol.value + (input._totalItemsValue - sol.valueWentThrough) < max.value)
        {
            continue;
        }
#endif

        if (sol.value > max.value)
        {
            max = sol;
        }

        if (sol.lastIndex == input.items.size() - 1)
        {
            continue;
        }

        int index = sol.lastIndex + 1;
        sol.lastIndex = index;

        Item item = input.items[index];
        if (item.weight + sol.weight <= input.capacity)
        {
            Solution added = copySolution(&sol);
            solutionAddItem(&added, &item);
            q.push(added);
        }
        // for ITEM_RATIO_HEURISTIC either put in or not, not both
#ifdef ITEM_RATIO_HEURISTIC
        else
        {
#endif
            solutionNotAddItem(&sol, &item);
            q.push(sol);
#ifdef ITEM_RATIO_HEURISTIC
        }
#endif
    }

#ifdef ITEM_RATIO_REDUX
    if (max.value < input._mostValuableItem.price)
    {
        Solution biggestItemSolution = copySolution(&emptySolution);
        solutionAddItem(&biggestItemSolution, &input._mostValuableItem);
        max = biggestItemSolution;
    }
#endif

    return max;
}

Solution solve(InputLine input)
{
#ifdef DYNAMIC_PROGRAMMING
    return solveDP(input);
#else
    return solveIterative(input);
#endif
}

int main()
{
    vector<InputLine> inputs = processInput();
    inputs.pop_back(); // For some reason this is loading the last entry twice
    for (InputLine input : inputs)
    {
        Solution sol = solve(input);
#ifndef DISABLE_OUTPUT
        printSolution(&sol);
#endif
    }
    return 0;
}