#/bin/sh
VERSION=NK
SIZE=15
FOLDER="results_2"
TIMES_OUT_FILE="${FOLDER}/times_${SIZE}_${VERSION}.txt"
RESULT_OUT_FILE="${FOLDER}/result_${SIZE}_${VERSION}_${TYPE}.txt"
SAMPLE_COUNT=10

# TYPES=("DP")
TYPES=("BNB" "HEURISTIC" "REDUX" "DP")
EPSILONS=(0.0 0.1 0.2)
# SIZES=("15")
SIZES=("15" "20" "22")
VERSIONS=("NK" "ZKC" "ZKW")

test () {
    make build-`echo "$TYPE" | awk '{print tolower($0)}'` > /dev/null
    echo "---${TYPE}" >> $TIMES_OUT_FILE
    for i in $(seq $SAMPLE_COUNT)
    do
        printf "."
        { time -p make IN_FILE="${VERSION}/${VERSION}${SIZE}_inst.dat" test > $RESULT_OUT_FILE ; } 2>> $TIMES_OUT_FILE
    done
    echo ""
}

test_dp () {
    for EPSILON in "${EPSILONS[@]}"
    do
        echo "---${TYPE}-E${EPSILON//.}" >> $TIMES_OUT_FILE
        make EPSILON=${EPSILON} build-`echo "$TYPE" | awk '{print tolower($0)}'` > /dev/null
        echo "Ɛ=${EPSILON}"
        RESULT_OUT_FILE="${FOLDER}/result_${SIZE}_${VERSION}_${TYPE}_E${EPSILON//.}.txt"
        for i in $(seq $SAMPLE_COUNT)
        do
            printf "."
            { time -p make IN_FILE="${VERSION}/${VERSION}${SIZE}_inst.dat" test > $RESULT_OUT_FILE ; } 2>> $TIMES_OUT_FILE
        done
        echo ""
    done
}

run_tests () {
    for SIZE in "${SIZES[@]}"
    do
        for VERSION in "${VERSIONS[@]}"
        do
            for TYPE in "${TYPES[@]}"
            do
                TIMES_OUT_FILE="${FOLDER}/times_${SIZE}_${VERSION}.txt"
                RESULT_OUT_FILE="${FOLDER}/result_${SIZE}_${VERSION}_${TYPE}.txt"
                echo "Running ${TYPE} ${VERSION}${SIZE}"
                if [ $TYPE = "DP" ]; then
                    test_dp
                else
                    test
                fi
            done
        done
    done
    
    make clean > /dev/null
}

run_tests